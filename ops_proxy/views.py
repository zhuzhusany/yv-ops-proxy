# coding=utf-8
import json
import time
import requests
import sys
import re
from common.utils import unicode_2_utf8
from django.http import HttpResponseRedirect
from common.mymako import render_mako_context
from bs4 import BeautifulSoup
from ops_proxy.models import Account
from django.forms.models import model_to_dict
from common.log import logger
from django.shortcuts import HttpResponse
from django.conf import settings

# Create your views here.

session = requests.Session()


def ops_proxy(request):
    method = request.method
    uri = request.path
    logger.error('full path=' + request.get_full_path())
    if settings.SITE_URL != "/":
        uri = uri.replace(settings.SITE_URL, "/")
    logger.debug('uri=' + uri + ',SITE_URL=' + settings.SITE_URL)
    if uri == "/":
        uri = "/dashboard/"
    headers = request.META
    if 'GET' == method:
        params = request.GET
    else:
        params = request.POST
    if Account.objects.exists():
        setting = Account.objects.all().order_by('-id')[:1]
        account = unicode_2_utf8(model_to_dict(setting.first()))
    else:
        # 如果取不到设置信息，则直接跳转至设置页面
        return HttpResponseRedirect('/settings/')
    ops_hosts = get_host(account['url'])
    ops_url = ops_hosts + uri
    logger.debug('!!!!!ops_url=' + ops_url)
    if 'GET' == method:
        ops_response = session.get(ops_url, data=params, headers=headers)
    else:
        ops_response = session.post(ops_url, data=params, headers=headers)

    action, csrf = get_csrf(ops_response.text)
    headers = make_proxy_headers(headers, ops_response.headers)
    print action, csrf
    login_data = account
    login_data["csrfmiddlewaretoken"] = csrf if csrf else ""
    print '-' * 200
    print ops_response.headers
    print '-' * 200
    if '/dashboard/auth/login/' == action:
        login_response = session.post(ops_hosts + action, data=login_data, headers=headers, timeout=600000)
        print 'login_response.status_code=' + str(login_response.status_code)
        return HttpResponse(proxy_response(login_response))
    print ops_response.status_code
    print ops_response.headers
    reload(sys)
    sys.setdefaultencoding('utf8')
    print '!!!!!!!!!Content-Type=' + ops_response.headers.get('Content-Type')
    return HttpResponse(proxy_response(ops_response), ops_response.headers.get('Content-Type'))


def proxy_response(response):
    reload(sys)
    sys.setdefaultencoding('utf8')
    if not response:
        return ''
    html = response.text
    if html:
        html = html.replace('/dashboard/', settings.SITE_URL + 'dashboard/')
    return html


def make_proxy_headers(hs, tar_hs):
    for k, v in tar_hs.items():
        if k == 'Set-Cookie':
            hs['Cookie'] = v

    return hs

def ops_proxy_settings(request):
    accounts = Account.objects.all()
    result = {'domain': 'default', 'username': 'admin', 'password': '', 'url': '', 'modify': 0}
    if accounts.count() > 0:
        for account in accounts:
            result['domain'] = account.domain
            result['username'] = account.username
            result['password'] = account.password
            result['url'] = account.url
    return render_mako_context(request, '/ops_proxy/proxy_settings.html', result)


def ops_proxy_settings_update(request):
    domain = request.POST.get("domain", "default")
    username = request.POST.get("username", "admin")
    password = request.POST.get("password", "")
    url = request.POST.get("url", "")

    if not password or not url:
        raise Exception("invalid parameter!url or password could not be empty!", 400)

    modify = 0
    if Account.objects.exists():
        accounts = Account.objects.all().order_by('-id')[:1]
        modify = 1
        for account in accounts:
            account.domain = domain
            account.username = username
            account.password = password
            account.url = url
            account.save()
    else:
        account = Account.objects.create(domain=domain, username=username, password=password, url=url)

    result = model_to_dict(account)
    result["modify"] = modify
    return render_mako_context(request, '/ops_proxy/proxy_settings.html', result)


def get_csrf(html):
    try:
        if html:
            doc = BeautifulSoup(html, "html.parser")
            dom_inputs = doc.find_all('input')
            if dom_inputs and len(dom_inputs) > 0:
                for i in dom_inputs:
                    if i['name'] == 'csrfmiddlewaretoken':
                        csrf = i['value']
                dom_form = doc.find('form')
                return dom_form['action'], csrf
            else:
                return None, None
        return None, None
    except:
        return None, None


def get_host(url):
    pattern = re.compile(r'(.*?)://(.*?)/', re.S)
    response = re.search(pattern, url)
    if response:
        return str(response.group(1)).strip() + '://' + str(response.group(2)).strip()
    else:
        return None


def get_location(response):
    host_obj = dict()
    locations = set()
    if 'Location' in response.headers.keys():
        url = response.headers.get('Location')
        print 'url=' + url
        if 'http' not in url:
            url = host_obj['header'] + '://' + host_obj['host']
            locations.add(url)
    return locations
