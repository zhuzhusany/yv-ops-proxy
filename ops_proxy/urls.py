# -*- coding: utf-8 -*-

from django.conf.urls import patterns

urlpatterns = patterns(
    'ops_proxy.views',
    (r'^$', 'ops_proxy_settings'),
    (r'^dashboard/.*$', 'ops_proxy'),
    # (r'^dashboard/.*$', 'ops_proxy'),
    (r'^settings/$', 'ops_proxy_settings'),
    (r'^settings/update/$', 'ops_proxy_settings_update'),
)
